#!/usr/bin/env python3
# utf-8

"""Create a Pytorch model from scratch that learns embeddings.
Initially train on the MovieLens database.
Compare the results with Jeremy Howard's.
The code is likely to be slow, as the model will use a one-hot encoded matrix multiplication as a lookup.
"""


from fastai.collab import *
from fastai.tabular import *

import torch
from torch import nn, optim
from torch.utils.data import Dataset, DataLoader, random_split
import torch.nn.functional as F
from torchvision import transforms

from pandas import get_dummies
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')

user, item, title = 'userId', 'movieId', 'title'
path = untar_data(URLs.ML_SAMPLE) # download a sample of the full dataset to begin with
ratings = pd.read_csv(path/'ratings.csv')
# ratings.head()

BATCH_SIZE = 32

test_len = len(movie_lens)//5
train_len = len(movie_lens) - test_len
train_data, test_data = random_split(movie_lens, [train_len, test_len])
trainloader = DataLoader(train_data, batch_size=BATCH_SIZE, shuffle=True, num_workers=4)
testloader = DataLoader(test_data, batch_size=BATCH_SIZE, shuffle=True, num_workers=4)

class CollabLearnDataset(Dataset):
    """A collaborative learner dataset class"""

    def __init__(self, users, items, ratings, transforms):
        """
        Parameters
        ----------
        users : pandas.Series
            A Series of user IDs (int or str)
        items : pandas.Series
            A Series of item IDs (int or str)
        ratings : pandas.Series
            A Series of ratings (floats)
        transform : function(s)
            To include one hot-encode and 'to_tensor' functions to convert 
            users and items to one hot-encoded tensors
        """
        self.users = get_dummies(users)
        self.items = get_dummies(items)
        self.ratings = ratings
        self.transforms = transforms

    def __len__(self):
        """Returns the number of observations"""
        return len( self.items)

    def __getitem__(self, idx):
        sample = {
            'user': self.users.iloc[idx], 
            'item': self.items.iloc[idx], 
            'rating': self.ratings[idx]
            }
        if self.transforms:
            sample = self.transforms(sample)
            
        return sample

class ToTensor:
    """Takes a sample from a CollabLearnDataset and converts one hot-encoded 
    item and user matrices to pytorch tensors
    """
    
    def __call__(self, sample):
        user, item, rating = sample['user'], sample['item'], sample['rating']
        
        transformed_sample = {
            'user': torch.tensor(user.values).type(torch.FloatTensor),
            'item': torch.tensor(item.values).type(torch.FloatTensor), 
            'rating': torch.tensor(rating).type(torch.FloatTensor)}
        return transformed_sample

movie_lens = CollabLearnDataset(ratings[user], ratings[item], ratings['rating'], ToTensor())
next_data = next(iter(movie_lens))


class CollabLearnerScratch(nn.Module):
    """A Pytorch model for learning embeddings"""
    
    def __init__(self, embed_dim=128):
        """
        Parameters
        ----------
        embed_dim : int
            The number of dimensions of the embedding
            Defaults to 128
        """
        super().__init__()
        self.embed_dim = embed_dim
        self.fc1 = nn.Linear(N_FEATURES, self.embed_dim) # input layer
        # PICK UP HERE: how do we stack the matrices so they learn together??
        self.fc2 = None
        
    def forward(self, x):
        x = x.view(x.shape[0], -1) # make sure input tensor is the correct shape
        x = logistic(self.fc1(x))
        return x
  


#%%
plt.plot([e for e in range(epochs)], train_loss, 'b');
plt.plot([e+1 for e in range(epochs)], test_loss, 'r');

###########################################################
# the full dataset, for later:
path=Config.data_path()/'ml-100k'
ratings = pd.read_csv(path/'u.data', delimiter='\t', header=None,
                      names=[user,item,'rating','timestamp'])

ratings.head()

movies = pd.read_csv(path/'u.item',  delimiter='|', encoding='latin-1', header=None,
                    names=[item, 'title', 'date', 'N', 'url', *[f'g{i}' for i in range(19)]])
movies.head()

len(ratings)
