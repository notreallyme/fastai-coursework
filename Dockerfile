FROM python:3.6-slim-stretch

RUN apt-get update && \
apt-get install -y python3-dev gcc

# install packages
RUN pip install torch_nightly -f https://download.pytorch.org/whl/nightly/cpu/torch_nightly.html && \
pip install fastai && \
pip install pandas && \
pip install sklearn && \
pip install seaborn && \
pip install plotly && \
pip install vega && \
pip install jupyter

# VOLUME ["/host"]
# WORKDIR /host
EXPOSE 8888

# start jupyter on launch
CMD ["cd", "jupyter"]
ENTRYPOINT [ "jupyter" ]
CMD ["notebook", "--NotebookApp.token=''", "--no-browser", "--ip=0.0.0.0", "--port=8888", "--allow-root", "--notebook-dir='home/jovyan/host-dir'" "--NotebookApp.disable_check_xsrf=True"]
