# fastai Coursework
## This repository contains my course work for fastai's "Deep Learning For Coders" course

The contents are divided by lesson and include trained models, working apps etc.

```bash
git clone git@bitbucket.org:notreallyme/fastai-coursework
docker build -t fastai fastai-coursework
cd fastai-coursework
sudo chmod +x run.sh
./run.sh
```
Open a browser at http://127.0.0.1:8888/  
The `run.sh` script mounts the home directory in `/jovyan/work/host-dir` and launches jupyter in `/jovyan/work`
