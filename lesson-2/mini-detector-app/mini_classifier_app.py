# coding: utf-8

from starlette.applications import Starlette
from starlette.responses import JSONResponse, HTMLResponse, RedirectResponse
from fastai.vision import (
    ImageDataBunch,
    Learner,
    load_learner,
    open_image,
    get_transforms,
    models,
    defaults
)
import torch
from pathlib import Path
from io import BytesIO
import sys
import uvicorn
import aiohttp
import asyncio
import numpy as np
import os

defaults.device = torch.device('cpu')

mini_learner = load_learner(".", "export.pkl")

async def get_bytes(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            return await response.read()

app = Starlette()

@app.route("/upload", methods=["POST"])
async def upload(request):
    data = await request.form()
    bytes = await (data["file"].read())
    prediction = predict_image_from_bytes(bytes)
    return   HTMLResponse(create_response_page(prediction))

@app.route("/classify-url", methods=["GET"])
async def classify_url(request):
    bytes = await get_bytes(request.query_params["url"])
    prediction = predict_image_from_bytes(bytes)
    return   HTMLResponse(create_response_page(prediction))

def predict_image_from_bytes(bytes):
    classes = ["a new mini", "an old mini"]
    img = open_image(BytesIO(bytes))
    _, pred_idx, _ = mini_learner.predict(img)
    return classes[pred_idx.item()]

def create_response_page(prediction):
    page_contents = """
    Classification succesful!
    <h4>It's """ +  prediction + "!\n<br>" + """
        </form>
        <form action="/">
            <input type="submit" value="Return to main page" />
        </form>
        """
    return page_contents
    
@app.route("/")
def form(request):
    return HTMLResponse(
        """
        <h1>Mini classifier</h1>
        <form action="/upload" method="post" enctype="multipart/form-data">
            Select image to upload:
            <input type="file" name="file">
            <input type="submit" value="Upload Image">
        </form>
        Or submit a URL:
        <form action="/classify-url" method="get">
            <input type="url" name="url">
            <input type="submit" value="Fetch and analyze image">
        </form>
    """)

@app.route("/form")
def redirect_to_homepage(request):
    return RedirectResponse("/")

if __name__ == "__main__":
    if "serve" in sys.argv:
        uvicorn.run(app, host="0.0.0.0", port=8008)
