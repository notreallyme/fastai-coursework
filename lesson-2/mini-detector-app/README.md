# Mini Classifier App

This app uses ResNet34 to classify old vs. new minis  
The backend uses Starlette

I have borrowed heavily from Simon Willison's [`Cougar or Not`](https://github.com/simonw/cougar-or-not) app

Comes with a Docker file :)

To create the conda env:   
```
conda create env --name starlette  
source activate starlette  
conda install -c pytorch -c fastai pytorch fastai  
pip3 install starlette uvicorn python-multipart aiohttp  
```
